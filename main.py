from selenium import webdriver
from selenium.webdriver.common.by import By


if __name__ == '__main__':
    """
    Main
    """
    # 크롬 드라이버 생성
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option('detach', True)
    driver = webdriver.Chrome(options=chrome_options)
    driver.maximize_window()    # 창 최대화
    driver.get('https://www.naver.com') # URL로 이동

    input = driver.find_element(By.XPATH, '//*[@id="query"]')
    input.send_keys('selenium')

    search = driver.find_element(By.XPATH, '//*[@id="sform"]/fieldset/button')
    search.click()

    print('end')
